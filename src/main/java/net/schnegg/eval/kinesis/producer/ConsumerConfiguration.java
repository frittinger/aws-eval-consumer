package net.schnegg.eval.kinesis.producer;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.kinesis.KinesisAsyncClient;

@Data
public class ConsumerConfiguration {

    private final String streamName;
    private final Region region;
    private final KinesisAsyncClient kinesisClient;

}
