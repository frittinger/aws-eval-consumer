package net.schnegg.eval.kinesis.producer;

import lombok.Data;
import lombok.NonNull;
import net.schnegg.eval.proto.Person;

@Data
public class SimplePerson {

    @NonNull
    private String name;
    @NonNull
    private String email;

    public Person getProto() {
        Person person = Person.newBuilder().setName(name).setEmail(email).build();
        return person;
    }
}
