package net.schnegg.eval.kinesis.producer;

import net.schnegg.eval.proto.Person;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class SimplePersonTest {

    private static final String NAME = "John Doe";
    private static final String EMAIL = "john@doe.com";

    @Test
    void getProto() {
        SimplePerson simple = new SimplePerson(NAME, EMAIL);
        Person proto = simple.getProto();
        Assertions.assertEquals(NAME, proto.getName());
        Assertions.assertEquals(EMAIL, proto.getEmail());
    }
}