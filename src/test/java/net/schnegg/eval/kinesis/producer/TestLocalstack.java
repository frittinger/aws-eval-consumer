package net.schnegg.eval.kinesis.producer;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.kinesis.producer.Attempt;
import com.amazonaws.services.kinesis.producer.KinesisProducer;
import com.amazonaws.services.kinesis.producer.KinesisProducerConfiguration;
import com.amazonaws.services.kinesis.producer.UserRecordResult;
import net.schnegg.eval.proto.Person;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.localstack.LocalStackContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.core.SdkSystemSetting;
import software.amazon.awssdk.http.nio.netty.NettyNioAsyncHttpClient;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.cloudwatch.CloudWatchAsyncClient;
import software.amazon.awssdk.services.dynamodb.DynamoDbAsyncClient;
import software.amazon.awssdk.services.kinesis.KinesisAsyncClient;
import software.amazon.awssdk.services.kinesis.model.*;
import software.amazon.kinesis.common.ConfigsBuilder;
import software.amazon.kinesis.coordinator.Scheduler;
import software.amazon.kinesis.retrieval.polling.PollingConfig;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static com.amazonaws.SDKGlobalConfiguration.DISABLE_CERT_CHECKING_SYSTEM_PROPERTY;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.testcontainers.containers.localstack.LocalStackContainer.Service.*;

@Testcontainers
class TestLocalstack {

    public static final String STREAM_NAME = "myStreamName";

    DockerImageName localstackImage = DockerImageName.parse("localstack/localstack:0.12.18");
    KinesisAsyncClient asyncClient;

    @Container
    LocalStackContainer localstack = new LocalStackContainer(localstackImage).withServices(KINESIS, DYNAMODB, CLOUDWATCH);

    @BeforeAll
    static void prepareTests() {

        System.setProperty(SdkSystemSetting.CBOR_ENABLED.property(), "false");
        System.setProperty(DISABLE_CERT_CHECKING_SYSTEM_PROPERTY, "true");

    }

    StreamStatus createTestStream() throws ExecutionException, InterruptedException {
        asyncClient = getKinesisClient();
        asyncClient.createStream(CreateStreamRequest.builder().streamName(STREAM_NAME).shardCount(1).build());

        // TODO rewrite with awaitility
        Thread.sleep(2000);

        CompletableFuture<DescribeStreamResponse> response = asyncClient.describeStream(DescribeStreamRequest.builder().streamName(STREAM_NAME).build());
        StreamDescription description = response.get().streamDescription();
        System.out.println("### Stream created: " + description.streamStatusAsString());
        System.out.println("### Stream ARN: " + description.streamARN());

        return description.streamStatus();
    }

    public KinesisProducer getKinesisProducer() {
        KinesisProducerConfiguration config = new KinesisProducerConfiguration();
        config.setRegion(localstack.getRegion());
        config.setCredentialsProvider(new AWSStaticCredentialsProvider(new BasicAWSCredentials(localstack.getAccessKey(), localstack.getSecretKey())));
        config.setMaxConnections(1);
        config.setRequestTimeout(6000); // 6 seconds
        config.setRecordMaxBufferedTime(5000); // 5 seconds
        config.setKinesisEndpoint(localstack.getEndpointOverride(KINESIS).getHost());
        config.setKinesisPort(localstack.getEndpointOverride(KINESIS).getPort());
        config.setVerifyCertificate(false);
        return new KinesisProducer(config);
    }

    public KinesisAsyncClient getKinesisClient() {
        return KinesisAsyncClient.builder()
                .credentialsProvider(StaticCredentialsProvider.create(AwsBasicCredentials.create(localstack.getAccessKey(), localstack.getSecretKey())))
                .region(Region.of(localstack.getRegion()))
                .endpointOverride(localstack.getEndpointOverride(KINESIS))
                .httpClient(NettyNioAsyncHttpClient.builder().build())
                .build();
    }

    private ConfigsBuilder getConfigsBuilder() {

        DynamoDbAsyncClient dynamoClient = getDynamoDbAsyncClient();
        CloudWatchAsyncClient cloudWatchClient = getCloudWatchAsyncClient();
        return new ConfigsBuilder(STREAM_NAME, STREAM_NAME, asyncClient, dynamoClient, cloudWatchClient, UUID.randomUUID().toString(), new PersonMessageProcessorFactory());
    }

    private CloudWatchAsyncClient getCloudWatchAsyncClient() {
        return CloudWatchAsyncClient.builder()
                .region(Region.of(localstack.getRegion()))
                .credentialsProvider(StaticCredentialsProvider.create(AwsBasicCredentials.create(localstack.getAccessKey(), localstack.getSecretKey())))
                .endpointOverride(localstack.getEndpointOverride(CLOUDWATCH))
                .httpClient(NettyNioAsyncHttpClient.builder().build())
                .build();
    }

    private DynamoDbAsyncClient getDynamoDbAsyncClient() {
        return DynamoDbAsyncClient.builder()
                .region(Region.of(localstack.getRegion()))
                .credentialsProvider(StaticCredentialsProvider.create(AwsBasicCredentials.create(localstack.getAccessKey(), localstack.getSecretKey())))
                .endpointOverride(localstack.getEndpointOverride(DYNAMODB))
                .httpClient(NettyNioAsyncHttpClient.builder().build())
                .build();
    }

    @Test
    void create_stream_creates_a_stream() throws ExecutionException, InterruptedException {
        assertTrue(localstack.isRunning());
        StreamStatus status = createTestStream();
        assertEquals(StreamStatus.ACTIVE, status);
    }

    @Test
    void given_person_messages_in_stream_then_person_messages_can_be_read() throws InterruptedException, ExecutionException, UnsupportedEncodingException {

        assertTrue(localstack.isRunning());

        StreamStatus status = createTestStream();
        assertEquals(StreamStatus.ACTIVE, status);

        writeTestMessages();

        ConfigsBuilder configsBuilder = getConfigsBuilder();

        Scheduler scheduler = new Scheduler(
                configsBuilder.checkpointConfig(),
                configsBuilder.coordinatorConfig(),
                configsBuilder.leaseManagementConfig(),
                configsBuilder.lifecycleConfig(),
                configsBuilder.metricsConfig(),
                configsBuilder.processorConfig(),
                configsBuilder.retrievalConfig().retrievalSpecificConfig(new PollingConfig(STREAM_NAME, asyncClient))
        );

        Thread schedulerThread = new Thread(scheduler);
        schedulerThread.setDaemon(true);
        schedulerThread.start();

        // TODO How to assert that the reading was succesfull? Store in DB and check the entry

    }

    private void writeTestMessages() throws InterruptedException, ExecutionException {
        KinesisProducer kinesis = getKinesisProducer();

        List<Future<UserRecordResult>> putFutures = new LinkedList<>();
        for (int i = 0; i < 2; i++) {
            Person johnDoe = Person.newBuilder()
                    .setName("John Doe")
                    .setEmail("john@doe.com")
                    .addPhones(Person.PhoneNumber.newBuilder().setNumber(String.valueOf(i)).build())
                    .build();

            putFutures.add(kinesis.addUserRecord(STREAM_NAME, "myPartitionKey", ByteBuffer.wrap(johnDoe.toByteArray())));
        }

        for (Future<UserRecordResult> f : putFutures) {
            UserRecordResult result = f.get(); // this does block
            if (result.isSuccessful()) {
                System.out.println("### Put record into shard " +
                        result.getShardId());
            } else {
                for (Attempt attempt : result.getAttempts()) {
                    System.out.println("### writing to kinesis failed: " + attempt.getErrorMessage());
                }
            }
        }
    }
}